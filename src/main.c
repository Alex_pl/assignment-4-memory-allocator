//
// Created by Александр Плетнёв on 18.12.2022.
//
#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"
#include "tests.h"

int main() {
    test1();
    test2();
    test3();
    test4();
    test5();
    return 0;
}