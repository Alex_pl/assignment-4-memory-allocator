//
// Created by Александр Плетнёв on 23.01.2023.
//

#include "tests.h"
#include "mem.h"
#include "mem_internals.h"

#define MAGIC_NUMBER 42
#define HEAP_MUNMAP munmap(heap, size_from_capacity((block_capacity){.bytes=REGION_MIN_SIZE}).bytes);
void test1() {
    void* heap = heap_init(REGION_MIN_SIZE);
    uint8_t* block = _malloc(MAGIC_NUMBER);
    _free(block);
    debug_heap(stdout, heap);
    printf("Test 1 complete\n");
    HEAP_MUNMAP
}
void test2() {
    void* heap = heap_init(REGION_MIN_SIZE);
    uint8_t* block1 = _malloc(MAGIC_NUMBER);
    uint8_t* block2 = _malloc(MAGIC_NUMBER);
    _free(block2);
    debug_heap(stdout, heap);
    printf("Test 2 complete\n");
    _free(block1);
    HEAP_MUNMAP
}
void test3() {
    void* heap = heap_init(REGION_MIN_SIZE);
    uint8_t* block1 = _malloc(MAGIC_NUMBER);
    uint8_t* block2 = _malloc(MAGIC_NUMBER);
    uint8_t* block3 = _malloc(MAGIC_NUMBER);
    _free(block1);
    debug_heap(stdout, heap);
    _free(block2);
    debug_heap(stdout, heap);
    printf("Test 3 complete\n");
    _free(block3);
    HEAP_MUNMAP
}
void test4() {
    void* heap = heap_init(REGION_MIN_SIZE);
    uint8_t* block = _malloc(MAGIC_NUMBER);
    printf("%s", block);
    debug_heap(stdout, heap);
    block = _malloc(REGION_MIN_SIZE * 2);
    debug_heap(stdout, heap);
    _free(block);
    HEAP_MUNMAP
}
void test5() {
    void* heap = heap_init(REGION_MIN_SIZE);
    munmap(heap, size_from_capacity((block_capacity){.bytes=REGION_MIN_SIZE}).bytes);
    void *new_heap = heap_init(REGION_MIN_SIZE);
    struct block_header *bh = (struct block_header *) new_heap;
    void *malloc = _malloc(bh->capacity.bytes);
    debug_heap(stdout, new_heap);
    void *malloc1 = mmap(bh->contents + bh->capacity.bytes, REGION_MIN_SIZE*2, PROT_READ | PROT_WRITE, MAP_PRIVATE, -1, 0);
    void *malloc2 = _malloc(MAGIC_NUMBER);
    debug_heap(stdout, new_heap);
    _free(malloc);
    _free(malloc2);
    munmap(malloc1, size_from_capacity((block_capacity) {.bytes=5000}).bytes);
    HEAP_MUNMAP
    printf("Test 5 complete\n");
    HEAP_MUNMAP
}